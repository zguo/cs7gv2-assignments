import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as anim

if __name__ == '__main__':
    # settings
    frame_rate = 30
    update_frequency = 1
    
    phase = 0
    amplitude = 5
    omega = 2

    plot_speed = 2
    max_length = 5
    gap = plot_speed / frame_rate
    max_count = int(max_length / gap)

    fig = plt.figure()
    ax = fig.add_subplot(projection='3d')
    xs = []
    ys = []

    def animate(frame):
        t = frame / frame_rate
        ax.clear()
        ax.set_ylim(-5, 5)
        ax.set_xlim(0, 6)
        ax.set_zlim(-5, 5)
        x = plot_speed * t
        ys.append(amplitude * np.sin(omega * x))
        if len(xs) < max_count:
            xs.append(x)
        if len(ys) > max_count:
            del(ys[0])
        ax.plot(xs, -1 * np.array(ys), zdir='z')
        ax.plot(xs, ys, zdir='y')
        fig.suptitle('electromagnetic field')

    animation = anim.FuncAnimation(fig, animate, interval=1000 / frame_rate)
    plt.show()