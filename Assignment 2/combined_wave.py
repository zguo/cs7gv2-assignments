import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as anim

class Point:
    points = []

    def __init__(self, collection, initial_pos, attenuation) -> None:
        self.position = (0, 0)
        self.initial_pos = initial_pos
        self.collection = collection
        self.attenuation = attenuation

    def update(self, frame):
        t = frame / frame_rate
        x = self.initial_pos[0]
        # using closed-form wave equation
        self.position = (self.initial_pos[0] + self.attenuation * R * np.cos(k * x - omega * t) + (1 - R) * np.cos(k * x + omega * t), self.initial_pos[1] + self.attenuation * R * np.cos(k * x - omega * t) + (1 - R) * np.cos(k * x + omega * t))
        self.collection.append(self.position)


if __name__ == '__main__':
    # settings
    frame_rate = 30
    update_frequency = 1
    num_points = 1000

    omega = 5
    R = 1
    k = 1

    xs = np.random.uniform(0, 8, num_points)
    ys = np.random.uniform(1, 5, num_points)
    attenuations = ((ys - 1) / 2) ** 2 * 0.1
    collection = []
    fig, ax = plt.subplots(subplot_kw={'aspect':'equal'})

    for i in range(num_points):
        Point.points.append(Point(collection, (xs[i], ys[i]), attenuations[i]))
    
    def animate(frame):
        ax.clear()
        collection.clear()
        for point in Point.points:
            point.update(frame)
        ax.set_ylim(1, 6)
        ax.set_xlim(0, 8)
        ax.set_aspect('equal')
        xs = []
        ys = []
        for pos in collection:
            xs.append(pos[0])
            ys.append(pos[1])
        ax.scatter(xs, ys, s=3)
        ax.set_title('combined wave')

    animation = anim.FuncAnimation(fig, animate, interval=1000 / frame_rate)
    plt.show()