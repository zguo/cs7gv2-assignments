import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as anim

class Point:
    points = []

    def __init__(self, collection, initial_pos) -> None:
        self.position = (0, 0)
        self.initial_pos = initial_pos
        self.collection = collection

    def update(self, frame):
        t = frame / frame_rate
        x = self.initial_pos[0]
        # using closed-form wave equation
        self.position = (self.initial_pos[0] + R * np.cos(k * x - omega * t) + (1 - R) * np.cos(k * x + omega * t), self.initial_pos[1])
        self.collection.append(self.position)


if __name__ == '__main__':
    # settings
    frame_rate = 30
    update_frequency = 1
    num_points = 500

    omega = 5
    R = 1
    k = 1

    xs = np.random.uniform(0, 8, num_points)
    ys = np.random.uniform(1, 5, num_points)
    collection = []
    fig, ax = plt.subplots(subplot_kw={'aspect':'equal'})

    for i in range(num_points):
        Point.points.append(Point(collection, (xs[i], ys[i])))
    
    def animate(frame):
        ax.clear()
        collection.clear()
        for point in Point.points:
            point.update(frame)
        ax.set_ylim(0, 6)
        ax.set_xlim(-1, 10)
        ax.set_aspect('equal')
        xs = []
        ys = []
        for pos in collection:
            xs.append(pos[0])
            ys.append(pos[1])
        ax.scatter(xs, ys, s=3)
        ax.set_title('longitudinal wave')

    animation = anim.FuncAnimation(fig, animate, interval=1000 / frame_rate)
    plt.show()