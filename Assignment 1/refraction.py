import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as anim
import matplotlib.patches as patches
from matplotlib.collections import PatchCollection
from scipy.optimize import fsolve

class WaveOrigin:
    origins = []

    def __init__(self, pos, collection, tangent_r = None, tangent_pos=None, update_frequency = 2, velocity = 3, frame_rate = 30, is_refraction = False, frame_offset = 0) -> None:
        self.pos = pos
        self.radii = []
        self.num_plots = 0
        self.update_frequency = update_frequency
        self.frame_rate = frame_rate
        self.velocity = velocity
        self.collection = collection
        self.max_radius = 10 * np.sqrt(2)
        self.is_refraction = is_refraction
        self.has_triggered_refraction = False
        self.frame_offset = frame_offset
        self.tangent_r = tangent_r
        self.tangent_pos = tangent_pos
    
    def update(self, frame):
        idx_to_remove = -1
        # decide whether to remove a plotting
        for i in range(len(self.radii)):
            self.radii[i] += self.velocity / self.frame_rate
            if self.radii[i] > self.max_radius:
                idx_to_remove = i
                continue
            else:
                if not self.is_refraction and not self.has_triggered_refraction:
                    if self.radii[i] >= self.tangent_r and self.tangent_pos[0] >= 0 and self.tangent_pos[0] <= 10:
                        # use the tangent point as the new wave origin
                        WaveOrigin.origins.append(WaveOrigin(self.tangent_pos, self.collection, update_frequency=self.update_frequency, velocity=1.5, frame_rate=self.frame_rate, is_refraction=True, frame_offset=frame))
                        self.has_triggered_refraction = True
                theta1 = -135 if not self.is_refraction else -(np.rad2deg(np.arctan(2)) + 90)
                theta2 = 45 if not self.is_refraction else np.rad2deg(np.arctan(0.5))
                wedge = patches.Wedge(self.pos, self.radii[i], theta1, theta2, facecolor='none', edgecolor='blue' if not self.is_refraction else 'red')
                self.collection.append(wedge)
        # remove the marked circle whose radius exceeds the max radius
        if idx_to_remove != -1:
            self.radii.pop(idx_to_remove)
            self.num_plots -= 1
        if (frame - self.frame_offset) % (self.update_frequency * self.frame_rate) == 1:
            # add a new plotting
            r = 0
            wedge = patches.Wedge(self.pos, r, 0, 1)
            self.collection.append(wedge)
            self.num_plots += 1
            self.radii.append(r)


# using scipy
def solve_tangent(pos):
    def func(i):
        x, y = i[0], i[1]
        return [
            (x - x0) ** 2 + (y - y0) ** 2 - r ** 2,
            0.5 * x - y - 5
        ]
    x0 = pos[0]
    y0 = pos[1]
    r = np.abs(0.5 * x0 - y0 - 5) / np.sqrt(1.25)
    x, y = fsolve(func, [0, 0])
    return (x, y, r)

if __name__ == '__main__':
    # settings
    frame_rate = 30
    update_frequency = 1
    velocity = 5

    collection = []
    fig, ax = plt.subplots(subplot_kw={'aspect':'equal'})
    
    for pos in ((x, x) for x in np.arange(-5, 6, 1)):
        m, n, r = solve_tangent(pos)
        WaveOrigin.origins.append(WaveOrigin(pos, collection, tangent_r=r, tangent_pos=(m, n), update_frequency=update_frequency, velocity=velocity, frame_rate=frame_rate))

    def animate(frame):
        ax.clear()
        collection.clear()
        for origin in WaveOrigin.origins:
            origin.update(frame)
        p = PatchCollection(collection, match_original=True)
        ax.add_collection(p)
        ax.set_ylim(-10, 0)
        ax.set_xlim(0, 10)
        ax.set_aspect('equal')
        ax.plot()
        x = np.arange(0, 15, 0.5)
        y = 0.5 * x - 5
        ax.plot(x, y, color='green')
        ax.set_title('refraction')

    animation = anim.FuncAnimation(fig, animate, interval=1000 / frame_rate)
    plt.show()