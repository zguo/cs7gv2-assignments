import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as anim
import matplotlib.patches as patches
from matplotlib.collections import PatchCollection

class WaveOrigin:
    origins = []

    def __init__(self, pos, collection, update_frequency = 2, velocity = 3, frame_rate = 30) -> None:
        self.pos = pos
        self.radii = []
        self.num_plots = 0
        self.update_frequency = update_frequency
        self.frame_rate = frame_rate
        self.velocity = velocity
        self.collection = collection
        self.max_radius = 10
    
    def update(self, frame):
        idx_to_remove = -1
        # determine whether to remove a plotting
        for i in range(len(self.radii)):
            self.radii[i] += self.velocity / self.frame_rate
            if self.radii[i] > self.max_radius:
                idx_to_remove = i
                continue
            else:
                circle = patches.Circle(self.pos, radius=self.radii[i])
                self.collection.append(circle)
        if idx_to_remove != -1:
            self.radii.pop(idx_to_remove)
            self.num_plots -= 1
        if frame % (self.update_frequency * self.frame_rate) == 1:
            # add a new plotting
            r = 0
            circle = patches.Circle(self.pos, radius=r)
            self.collection.append(circle)
            self.num_plots += 1
            self.radii.append(r)

if __name__ == '__main__':
    # settings
    frame_rate = 30
    update_frequency = 1
    velocity = 3

    collection = []
    fig, ax = plt.subplots(subplot_kw={'aspect':'equal'})
    
    WaveOrigin.origins = [WaveOrigin(pos, collection, update_frequency=update_frequency, velocity=velocity, frame_rate=frame_rate) for pos in ((x, 0) for x in np.linspace(-5, 5, 10))]
    
    def animate(frame):
        ax.clear()
        collection.clear()
        for origin in WaveOrigin.origins:
            origin.update(frame)
        p = PatchCollection(collection, facecolor='none', edgecolor='blue')
        ax.add_collection(p)
        ax.set_ylim(0, 10)
        ax.set_xlim(-10, 10)
        ax.set_aspect('equal')
        ax.plot()
        ax.set_title('wave front')

    animation = anim.FuncAnimation(fig, animate, interval=1000 / frame_rate)
    plt.show()