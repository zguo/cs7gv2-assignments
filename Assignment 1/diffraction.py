import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as anim
import matplotlib.patches as patches
from matplotlib.collections import PatchCollection

class WaveOrigin:
    origins = []

    def __init__(self, pos, collection, tangent_r = None, tangent_pos=None, update_frequency = 2, velocity = 3, frame_rate = 30, is_diffraction = False, frame_offset = 0) -> None:
        self.pos = pos
        self.radii = []
        self.num_plots = 0
        self.update_frequency = update_frequency
        self.frame_rate = frame_rate
        self.velocity = velocity
        self.collection = collection
        self.max_radius = tangent_r
        self.is_diffraction = is_diffraction
        self.has_triggered_diffraction = False
        self.frame_offset = frame_offset
        self.tangent_r = tangent_r
        self.tangent_pos = tangent_pos
    
    def update(self, frame):
        idx_to_remove = -1
        # decide whether to remove a plotting
        for i in range(len(self.radii)):
            self.radii[i] += self.velocity / self.frame_rate
            if self.radii[i] > self.max_radius:
                idx_to_remove = i
                continue
            else:
                if not self.is_diffraction and not self.has_triggered_diffraction:
                    if self.radii[i] >= self.tangent_r and self.tangent_pos[0] >= -2 and self.tangent_pos[0] <= 2:
                        # use the tangent point as the new wave origin
                        WaveOrigin.origins.append(WaveOrigin(self.tangent_pos, self.collection, tangent_r=5, update_frequency=self.update_frequency, velocity=self.velocity, frame_rate=self.frame_rate, is_diffraction=True, frame_offset=frame))
                        self.has_triggered_diffraction = True
                theta1 = 0
                theta2 = 180
                wedge = patches.Wedge(self.pos, self.radii[i], theta1, theta2, facecolor='none', edgecolor='blue' if not self.is_diffraction else 'red')
                self.collection.append(wedge)
        # remove the marked circle whose radius exceeds the max radius
        if idx_to_remove != -1:
            self.radii.pop(idx_to_remove)
            self.num_plots -= 1
        if (frame - self.frame_offset) % (self.update_frequency * self.frame_rate) == 1:
            # add a new plotting
            r = 0
            wedge = patches.Wedge(self.pos, r, 0, 1)
            self.collection.append(wedge)
            self.num_plots += 1
            self.radii.append(r)

if __name__ == '__main__':
    # settings
    frame_rate = 30
    update_frequency = 1
    velocity = 5

    collection = []
    fig, ax = plt.subplots(subplot_kw={'aspect':'equal'})
    
    for pos in ((x, 0) for x in np.arange(-5, 6, 1)):
        WaveOrigin.origins.append(WaveOrigin(pos, collection, tangent_pos=(pos[0], 5), tangent_r=5, update_frequency=update_frequency, velocity=velocity, frame_rate=frame_rate))

    def animate(frame):
        ax.clear()
        collection.clear()
        for origin in WaveOrigin.origins:
            origin.update(frame)
        p = PatchCollection(collection, match_original=True)
        ax.add_collection(p)
        ax.set_ylim(0, 10)
        ax.set_xlim(-5, 5)
        ax.set_aspect('equal')
        ax.plot()
        x1 = np.arange(-5, -1.5, 0.5)
        x2 = np.arange(2, 5.5, 0.5)
        y1 = np.ones(x1.size) * 5
        y2 = np.ones(x2.size) * 5
        ax.plot(x1, y1, color='green')
        ax.plot(x2, y2, color='green')
        ax.set_title('diffraction')

    animation = anim.FuncAnimation(fig, animate, interval=1000 / frame_rate)
    plt.show()