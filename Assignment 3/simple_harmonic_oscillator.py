import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as anim
import matplotlib.patches as patches

if __name__ == '__main__':
    # settings
    frame_rate = 30
    update_frequency = 1
    
    K = 300
    m = 5
    omega = np.sqrt(K/m)
    phase = 0
    amplitude = 1.5

    period = 2 * np.pi / omega
    plot_speed = 3
    width = 3
    height = 3
    wave_length = 2
    max_count = np.floor(2 * period * frame_rate).astype(int)
    velocity = wave_length / period

    fig = plt.figure()
    ax1 = fig.add_subplot(1, 2, 1)
    ax2 = fig.add_subplot(1, 2, 2)
    ys = []
    xs = []

    def animate(frame):
        t = frame / frame_rate
        ax1.clear()
        ax2.clear()
        ax1.set_ylim(-5, 5)
        ax1.set_xlim(-5, 5)
        ax2.set_ylim(-np.ceil(amplitude), np.ceil(amplitude))
        ax2.set_xlim(0, 4)
        ax1.set_aspect('equal')
        ax2.set_aspect('equal')
        pos_y = amplitude * (np.cos(omega * t + phase)) - height / 2
        anchor_pos = (-1 * width / 2, pos_y)
        patch = patches.Rectangle(anchor_pos, width=width, height=height, facecolor='none', edgecolor='blue')
        ax1.add_patch(patch)
        ax1.plot()
        ys.insert(0, pos_y + height / 2)
        if len(xs) < max_count:
            xs.append(velocity * t)
        if len(ys) > max_count:
            del(ys[-1])
        ax2.plot(xs, ys)
        fig.suptitle('simple harmonic oscillator')

    animation = anim.FuncAnimation(fig, animate, interval=1000 / frame_rate)
    plt.show()